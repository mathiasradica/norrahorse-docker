const { task, src, dest, watch, series } = require("gulp");
const less = require("gulp-less");
 
task("less", function(){
	return src("./public/css/*.less")
        .pipe(less())
        .pipe(dest("./public/css"));
});

task("watch", function () {
	watch("./public/css/*.less", series("less"));
});

task("default", series("less", "watch"));