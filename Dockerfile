FROM php:7.4-apache
COPY ./ /var/www/html
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
WORKDIR /var/www/html
RUN apt-get update && \
    apt-get -y install git && \
    apt-get -y install nodejs && \
    apt-get -y install npm && \
    npm install --global gulp-cli &&\
    npm install gulp && \
    npm install gulp-less && \
    docker-php-ext-install pdo_mysql
CMD a2enmod rewrite && apache2ctl -D FOREGROUND